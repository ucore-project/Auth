<?php

namespace uCore\Auth\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateTableUser extends Migration
{
	public function up()
	{
		$this->forge
			->addField([
				'id' => [
					'type' => 'int',
					'constraint' => 11,
					'unsigned' => TRUE,
					'auto_increment' => TRUE,
				],
				'email' => [
					'type' => 'varchar',
					'constraint' => 255,
				],
				'name' => [
					'type' => 'varchar',
					'constraint' => 255,
				],
				'password' => [
					'type' => 'varchar',
					'constraint' => 255,
				],
				'status' => [
					'type' => 'enum',
					'constraint' => ['inactive', 'active'],
					'default' => 'inactive',
				],
				'levelId' => [
					'type' => 'int',
					'constraint' => 11,
					'unsigned' => TRUE,
				],
				'createdAt' => [
					'type' => 'datetime',
					'null' => TRUE,
				],
				'updatedAt' => [
					'type' => 'datetime',
					'null' => TRUE,
				],
			])
			->addPrimaryKey('id')
			->addForeignKey('levelId', 'user_level', 'id', 'CASCADE', 'CASCADE')
			->createTable('user');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->db->disableForeignKeyChecks();

		$this->forge->dropTable('user');

		$this->db->enableForeignKeyChecks();
	}
}
