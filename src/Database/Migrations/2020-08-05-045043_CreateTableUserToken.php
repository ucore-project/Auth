<?php

namespace uCore\Auth\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateTableUserToken extends Migration
{
	public function up()
	{
		$this->forge
			->addField([
				'id' => [
					'type' => 'int',
					'constraint' => 11,
					'unsigned' => TRUE,
					'auto_increment' => TRUE,
				],
				'userId' => [
					'type' => 'int',
					'constraint' => 11,
					'unsigned' => TRUE,
				],
				'token' => [
					'type' => 'varchar',
					'constraint' => 255,
				],
				'createdAt' => [
					'type' => 'datetime',
					'null' => TRUE,
				],
			])
			->addPrimaryKey('id')
			->addForeignKey('userId', 'user', 'id', 'CASCADE', 'CASCADE')
			->createTable('user_token');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->db->disableForeignKeyChecks();

		$this->forge->dropTable('user_token');

		$this->db->enableForeignKeyChecks();
	}
}
