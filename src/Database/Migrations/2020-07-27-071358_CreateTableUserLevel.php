<?php

namespace uCore\Auth\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateTableUserLevel extends Migration
{
	public function up()
	{
		$this->forge
			->addField([
				'id' => [
					'type' => 'int',
					'constraint' => 11,
					'unsigned' => TRUE,
					'auto_increment' => TRUE,
				],
				'code' => [
					'type' => 'varchar',
					'constraint' => 255,
				],
				'name' => [
					'type' => 'varchar',
					'constraint' => 255,
				],
				'description' => [
					'type' => 'text',
					'null' => TRUE,
				],
				'isSuperuser' => [
					'type' => 'tinyint',
					'constraint' => 1,
					'default' => 0,
				],
				'createdAt' => [
					'type' => 'datetime',
					'null' => TRUE,
				],
				'updatedAt' => [
					'type' => 'datetime',
					'null' => TRUE,
				],
			])
			->addPrimaryKey('id')
			->createTable('user_level');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->db->disableForeignKeyChecks();

		$this->forge->dropTable('user_level');

		$this->db->enableForeignKeyChecks();
	}
}
