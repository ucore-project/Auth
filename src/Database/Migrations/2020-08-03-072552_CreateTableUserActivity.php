<?php

namespace uCore\Auth\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateTableUserActivity extends Migration
{
	public function up()
	{
		$this->forge
			->addField([
				'id' => [
					'type' => 'int',
					'constraint' => 11,
					'unsigned' => TRUE,
					'auto_increment' => TRUE,
				],
				'userId' => [
					'type' => 'int',
					'constraint' => 11,
					'unsigned' => TRUE,
				],
				'name' => [
					'type' => 'varchar',
					'constraint' => 255,
				],
				'description' => [
					'type' => 'text',
					'null' => TRUE,
				],
				'createdAt' => [
					'type' => 'datetime',
					'null' => TRUE,
				],
			])
			->addPrimaryKey('id')
			->addForeignKey('userId', 'user', 'id', 'CASCADE', 'CASCADE')
			->createTable('user_activity');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->db->disableForeignKeyChecks();

		$this->forge->dropTable('user_activity');

		$this->db->enableForeignKeyChecks();
	}
}
