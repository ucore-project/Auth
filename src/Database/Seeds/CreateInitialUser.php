<?php

namespace uCore\Auth\Database\Seeds;

use uCore\Auth\Models\UserModel;
use CodeIgniter\Database\Seeder;
use uCore\Auth\Models\UserLevelModel;

class CreateInitialUser extends Seeder
{
	public function run()
	{
		$this->registerLevels();
		$this->registerUser();
	}

	protected function registerLevels()
	{
		$model = new UserLevelModel();

		$groups = [
			[
				'code' => 'superuser',
				'name' => 'Superuser',
				'description' => 'Has all access',
				'isSuperuser' => TRUE,
			],
			[
				'code' => 'admin',
				'name' => 'Administrator',
				'description' => 'Has all administrative access',
				'isSuperuser' => FALSE,
			],
			[
				'code' => 'user',
				'name' => 'User',
				'description' => 'Has limited access',
				'isSuperuser' => FALSE,
			],
		];

		foreach ($groups as $group) {
			if (!$model->domain([['code', '=', $group['code']]])->first()) {
				$model->insert($group);
			}
		}
	}

	protected function registerUser()
	{
		$levelModel = new UserLevelModel();
		$userModel = new UserModel();

		$users = [
			[
				'email' => 'auliayf@gmail.com',
				'name' => 'Aulia Yusuf Fachrezy',
				'password' => '123',
				'status' => 'active',
				'levelId' => dot_search('id', $levelModel->domain([['code', '=', 'superuser']])->first()),
			],
		];

		foreach ($users as $user) {
			if (!$userModel->domain([['email', '=', $user['email']]])->first()) {
				$userModel->insert($user);
			}
		}
	}
}
