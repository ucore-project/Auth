<?php

namespace uCore\Auth\Models;

use Exception;
use uCore\Core\Entities\Entity;
use uCore\Core\Models\Model;

class UserModel extends Model
{
    /**
     * @var UserLevelModel
     */
    protected $levelModel;

    public const STATUS = ['inactive', 'active'];

    public function __construct()
    {
        parent::__construct();

        $this->levelModel = new UserLevelModel();

        $this->table = 'user';
        $this->useTimestamps = TRUE;

        $this->allowedFields = [
            'email',
            'name',
            'password',
            'status',
            'levelId',
            'createdAt',
            'updatedAt',
        ];

        $this->validationRules = [
            'email' => [
                'label' => 'Email',
                'rules' => "required|valid_email|max_length[255]|is_unique[{$this->table}.email,id,{id}]",
            ],
            'name' => [
                'label' => 'Name',
                'rules' => 'required|max_length[255]',
            ],
            'password' => [
                'label' => 'Password',
                'rules' => 'required_without[id]|max_length[255]',
            ],
            'status' => [
                'label' => 'Status',
                'rules' => 'required|in_list[' . implode(',', self::STATUS) . ']',
            ],
            'levelId' => [
                'label' => 'Level',
                'rules' => 'required',
            ],
        ];

        $this->beforeInsert = ['hashPassword'];
        $this->beforeUpdate = ['hashPassword'];
    }

    protected function rowFormatter(Entity $row): Entity
    {
        if (!empty($levelId = dot_search('levelId', $row))) {
            $row->level = $this->levelModel->find($levelId);
        }

        return $row;
    }

    protected function hashPassword(array $data): array
    {
        if (empty(dot_array_search('data.password', $data))) {
            unset($data['data']['password']);

            return $data;
        };

        $data['data']['password'] = password_hash($data['data']['password'], PASSWORD_DEFAULT);

        return $data;
    }

    public function login(?string $email = NULL, ?string $password = NULL): ?array
    {
        $user = $this->domain([
            ['TRIM(LOWER(email))', '=', trim(strtolower($email))]
        ])->first();

        if ($user) {
            if (password_verify($password, $user->password)) {
                if ($user->status == 'active') {
                    $tokenModel = new UserTokenModel();
                    $token = password_hash("{$user->id}." . time(), PASSWORD_DEFAULT);

                    $tokenModel->insert([
                        'userId' => $user->id,
                        'token' => $token,
                    ]);

                    return [
                        'token' => $token,
                        'user' => $user,
                    ];
                }
            }
        }

        throw new Exception("Invalid email/password");
    }

    public function verifyToken(?string $token = NULL): ?Entity
    {
        $tokenModel = new UserTokenModel();
        $userId = dot_search('userId', $tokenModel->domain([['token', '=', $token]])->first());

        if (!$userId) {
            return NULL;
        }

        return new Entity([
            'token' => $token,
            'user' => $this->find($userId),
        ]);
    }
}
