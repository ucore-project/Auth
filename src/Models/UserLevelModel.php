<?php

namespace uCore\Auth\Models;

use uCore\Core\Entities\Entity;
use uCore\Core\Models\Model;

class UserLevelModel extends Model
{

    public function __construct()
    {
        parent::__construct();

        $this->table = 'user_level';
        $this->useTimestamps = TRUE;

        $this->allowedFields = [
            'code',
            'name',
            'description',
            'isSuperuser',
            'createdAt',
            'updatedAt',
        ];

        $this->validationRules = [
            'code' => [
                'label' => 'Code',
                'rules' => 'required|max_length[255]',
            ],
            'name' => [
                'label' => 'Name',
                'rules' => 'required|max_length[255]',
            ],
            'description' => [
                'label' => 'Description',
                'rules' => 'permit_empty',
            ],
            'isSuperuser' => [
                'label' => 'Is Superuser',
                'rules' => 'permit_empty',
            ],
        ];
    }

    protected function rowFormatter(Entity $row): Entity
    {
        if (property_exists($row, 'isSuperuser')) {
            $row->isSuperuser = (bool) $row->isSuperuser;
        }

        return $row;
    }

    /**
     * Get level ids from code(s)
     *
     * @param string|array $code
     * @return array|null
     */
    public function idOf($code): ?array
    {
        return array_map(
            function (Entity $level) {
                return $level->id;
            },
            $this->domain([['code', is_array($code) ? "IN" : "=", $code]])->findAll()
        );
    }
}
