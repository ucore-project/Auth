<?php

namespace uCore\Auth\Models;

use uCore\Core\Models\Model;

class UserActivityModel extends Model
{

    public function __construct()
    {
        parent::__construct();

        $this->table = 'user_activity';
        $this->useTimestamps = TRUE;
        $this->updatedField = NULL;

        $this->allowedFields = [
            'userId',
            'name',
            'description',
            'createdAt',
        ];

        $this->validationRules = [
            'userId' => [
                'label' => 'User ID',
                'rules' => 'required|max_length[11]',
            ],
            'name' => [
                'label' => 'Activity',
                'rules' => 'required|max_length[255]',
            ],
            'description' => [
                'label' => 'Detail',
                'rules' => 'permit_empty',
            ],
        ];
    }
}
