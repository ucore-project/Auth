<?php

namespace uCore\Auth\Models;

use uCore\Core\Models\Model;

class UserTokenModel extends Model
{

    public function __construct()
    {
        parent::__construct();

        $this->table = 'user_token';
        $this->useTimestamps = TRUE;
        $this->updatedField = NULL;

        $this->allowedFields = [
            'userId',
            'token',
            'createdAt',
        ];

        $this->validationRules = [
            'userId' => [
                'label' => 'User ID',
                'rules' => 'required|max_length[11]',
            ],
            'token' => [
                'label' => 'Token',
                'rules' => 'required|max_length[255]',
            ],
        ];

        $this->afterInsert = ['trackActivity'];
    }

    protected function trackActivity(array $data)
    {
        $activityModel = new UserActivityModel();

        $activityModel->insert([
            'userId' => dot_search('data.userId', $data),
            'name' => 'Logged In',
            'description' => 'User logged in',
        ]);
    }
}
