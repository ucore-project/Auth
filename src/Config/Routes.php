<?php

/**
 * @var \CodeIgniter\Router\RouteCollection $routes
 */

$routes->group('auth', ['namespace' => 'uCore\Auth\Controllers'], function (\CodeIgniter\Router\RouteCollection $routes) {
    $routes->resource('check', ['only' => 'index']);
    $routes->resource('login', ['only' => 'create']);
    $routes->resource('logout', ['only' => 'delete']);

    $routes->resource('users', ['except' => ['new', 'edit']]);
});
