<?php

namespace uCore\Auth\API;

use CodeIgniter\API\ResponseTrait;
use uCore\Auth\Models\UserLevelModel;
use uCore\Auth\Models\UserModel;
use uCore\Core\Entities\Entity;

trait AuthenticationTrait
{
    use ResponseTrait;

    /**
     * Ensure request is authenticated
     *
     * @param array|string|null $level
     * @return Entity|null
     */
    protected function ensureAuthenticated($level = NULL): ?Entity
    {
        $levelModel = new UserLevelModel();
        $userModel = new UserModel();
        $token = str_replace("Bearer ", "", $this->request->getHeaderLine('Authorization') ?: $this->request->getGet('auth_token'));

        try {
            $session = $userModel->verifyToken($token);

            if (!$session) {
                $this->failUnauthorized()->send();
                exit();
            }

            $userLevel = dot_search('user.level', $session);

            if (!empty($level) && dot_search('isSuperuser', $userLevel, FALSE) !== TRUE) {
                $levelIds = $levelModel->idOf($level);

                if (!in_array($userLevel->id, $levelIds)) {
                    $this->failForbidden()->send();
                    exit();
                }
            }

            return $session;
        } catch (\Exception $e) {
            $this->failUnauthorized()->send();
            exit();
        }
    }
}
