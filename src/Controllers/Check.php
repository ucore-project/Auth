<?php

namespace uCore\Auth\Controllers;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\RESTful\ResourceController;
use Psr\Log\LoggerInterface;
use uCore\Auth\API\AuthenticationTrait;

class Check extends ResourceController
{
    use AuthenticationTrait;

    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        parent::initController($request, $response, $logger);
    }

    public function index()
    {
        return $this->respond(
            $this->ensureAuthenticated()
        );
    }
}
