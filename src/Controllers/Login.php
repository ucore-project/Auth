<?php

namespace uCore\Auth\Controllers;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\RESTful\ResourceController;
use Psr\Log\LoggerInterface;
use uCore\Auth\API\AuthenticationTrait;

class Login extends ResourceController
{
    use AuthenticationTrait;

    /**
     * @var \uCore\Auth\Models\UserModel
     */
    protected $model;

    /**
     * @var string
     */
    protected $modelName = 'uCore\Auth\Models\UserModel';

    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        parent::initController($request, $response, $logger);
    }

    public function create()
    {
        try {
            return $this->respond(
                $this->model->login(
                    $this->request->getPost('email'),
                    $this->request->getPost('password')
                )
            );
        } catch (\Exception $e) {
            return $this->failNotFound($e->getMessage());
        }
    }
}
