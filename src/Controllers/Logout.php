<?php

namespace uCore\Auth\Controllers;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\RESTful\ResourceController;
use Psr\Log\LoggerInterface;
use uCore\Auth\API\AuthenticationTrait;

class Logout extends ResourceController
{
    use AuthenticationTrait;

    /**
     * @var \uCore\Auth\Models\UserTokenModel
     */
    protected $model;

    /**
     * @var string
     */
    protected $modelName = 'uCore\Auth\Models\UserTokenModel';

    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        parent::initController($request, $response, $logger);
    }

    public function delete($id = null)
    {
        $session = $this->ensureAuthenticated();

        $res = $this->model->domain([
            ['userId', '=', $id],
            ['token', '=', dot_search('token', $session)]
        ])->first();

        if (!$res) {
            return $this->failNotFound();
        }

        $this->model->delete($res->id);

        return $this->respondDeleted($session);
    }
}
