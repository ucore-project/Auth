# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [2.0.0](https://gitlab.com/ucore-project/Auth/compare/v1.0.0...v2.0.0) (2020-08-21)


### ⚠ BREAKING CHANGES

* use uCore\Core\Entities\Entity as return type

### Bug Fixes

* use uCore\Core\Entities\Entity as return type ([7b1f0ed](https://gitlab.com/ucore-project/Auth/commit/7b1f0edf22d244e08b0ec777e586dd9cec7ed14b))

## 1.0.0 (2020-08-21)


### Features

* add uCore\Auth ([89566a7](https://gitlab.com/ucore-project/Auth/commit/89566a73276180d3e66c891b3d47082970ad4ca9))
